const express = require('express');
const router = express.Router();
const { biodata } = require('../controllers');



router.get('/', biodata.getAll);
router.post('/', biodata.create);
router.put('/:id', biodata.update);
router.delete('/:id', biodata.delete);


module.exports = router;