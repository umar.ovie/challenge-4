const { histories } = require('../models');
module.exports = {
    getAll: async (req, res) => {
        try {
            const histories = await histories.findAll();
            return res.status(200).json({
                jsonapi: {
                    version: "1.0",
                },
                meta: {
                    author: 'Muhammad Umar Mansyur',
                    copyright: '2022 ~ BE JavaScript Binar Academy',
                },
                status: 200,
                message: 'Data berhasil ditampilkan',
                data: histories
            });
        } catch (error) {
            console.log(error);
        }
    },
    create: async (req, res) => {
        try {
            const { user_id, question_id, answer } = req.body;
            const histories = await histories.create({
                user_id, question_id, answer
            });
            return res.status(201).json({
                jsonapi: {
                    version: "1.0",
                },
                meta: {
                    author: 'Muhammad Umar Mansyur',
                    copyright: '2022 ~ BE JavaScript Binar Academy',
                },
                status: 201,
                message: 'Data berhasil ditambahkan',
                data: histories
            });
        } catch (error) {
            console.log(error);
        }
    },
    update: async (req, res) => {
        try {
            const { id } = req.params;
            const { user_id, question_id, answer } = req.body;
            const histories = await histories.update({
                user_id, question_id, answer
            }, {
                where: {
                    id
                }
            });
            return res.status(200).json({
                jsonapi: {
                    version: "1.0",
                },
                meta: {
                    author: 'Muhammad Umar Mansyur',
                    copyright: '2022 ~ BE JavaScript Binar Academy',
                },
                status: 200,
                message: 'Data berhasil diubah',
                data: histories
            });
        } catch (error) {
            console.log(error);
        }
    },
    delete: async (req, res) => {
        try {
            const { id } = req.params;
            const histories = await histories.destroy({
                where: {
                    id
                }
            });
            return res.status(200).json({
                jsonapi: {
                    version: "1.0",
                },
                meta: {
                    author: 'Muhammad Umar Mansyur',
                    copyright: '2022 ~ BE JavaScript Binar Academy',
                },
                status: 200,
                message: 'Data berhasil dihapus',
                data: histories
            });
        } catch (error) {
            console.log(error);
        }
    }
}
